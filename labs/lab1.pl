directTrain(avon, braintree).
directTrain(quincy,avon).
directTrain(newton,boston).
directTrain(boston,avon).
directTrain(braintree,milton).
directTrain(westwood,newton).
directTrain(canton, westwood).

/* 
 * travelBetween solution
 * 
 * A - Start Location
 * B - End Location
 * Y - Intermediate Location
*/

travelBetween(A,B):-directTrain(A,B);directTrain(B,A).
travelBetween(A,B):-directTrain(A,Y),travelBetween(Y,B);directTrain(B,Y),travelBetween(Y,A).

tran(eine,one).
tran(zwei,two).
tran(drei,three).
tran(vier,four).
tran(funf,five).
tran(sechs,six).
tran(sieben,seven).
tran(acht,eight).
tran(neun,nine).

/* 
 * listtran solution
 *
 * G - German word(s)
 * E - English word(s)
 * Z - Next German word
 * Y - Next English word
*/
listtran([],[]).
listtran([G|Z],[E|Y]):-tran(G,E),listtran(Z,Y).

/* logic puzzle */
/* Animals! http://cf.ltkcdn.net/kids/files/1953-Animal-Lovers-Logic-Puzzle.pdf */

animal(unicorn).
animal(dragon).
animal(seaserpent).
animal(manatee).

/* logic puzzle */
/* Animals! http://cf.ltkcdn.net/kids/files/1953-Animal-Lovers-Logic-Puzzle.pdf */

animal(unicorn).
animal(dragon).
animal(seaserpent).
animal(manatee).

notlivesinwater(unicorn).
notlivesinwater(dragon).

nonsmoking(unicorn).
nonsmoking(seaserpent).
nonsmoking(manatee).

startswith(u,unicorn).
startswith(d,dragon).
startswith(m,manatee).
startswith(s,seaserpent).

flies(dragon).

dan(X):-notlivesinwater(X),not(startswith(d,X)).
melody(X):-nonsmoking(X),not(startswith(m,X)).
sarah(X):-flies(X),not(startswith(s,X)).
uli(X):-animal(X),not(startswith(u,X)).

determine(W,X,Y,Z):-dan(W),sarah(X),melody(Y),uli(Z),not(W=X),not(W=Y),not(W=Z),not(X=Y),not(X=Z),not(Y=Z).


