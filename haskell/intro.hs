--main = do
--	name <- getLine
--	putStr ("Hello, " ++ name ++ "\n")

-- This is a comment
-- This is using a fold (aka a reduce)
-- summation x = foldl (+) 0 x

-- Implement average
-- average :: [numbers] -> number
average x = (sum x) / (fromIntegral(length x))

-- Implement min (minimum element)
-- min :: [comparables things] -> one thing (the minimum, hence the name)
-- minim x = foldl1 min x

-- Implement max (maximum element)
-- max :: [comparable things] -> one thing (the maximum, hence the name)
-- maxim x = foldl1 max x