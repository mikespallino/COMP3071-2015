import org.antlr.v4.runtime.tree.ParseTree;

public class Calculator extends ExprBaseVisitor<Double> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override
	public Double visitProg(ExprParser.ProgContext ctx) {
		StringBuilder builder = new StringBuilder();
		int i = 0;
		Double res = 0d;
		for (ParseTree kid : ctx.children) {
			if (kid == ctx.NEWLINE(i)) {
				builder.append("\n");
				i++;
			} else {
				res = this.visit(kid);
				builder.append(res);
			}
		}
		return res;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override
	public Double visitExpr(ExprParser.ExprContext expression) {
		StringBuilder builder = new StringBuilder();
		Double a = 0d, b = 0d, eval = 0d;
		if (expression.number != null) {
			eval = new Double(expression.number.getText());
			builder.append(eval);
		}
		if (expression.sub != null) {
			eval = this.visit(expression.sub);
			builder.append("(" + eval + ")");
		}
		if (expression.left != null) {
			a = this.visit(expression.left);
			builder.append(a);
		}
		if (expression.right != null) {
			b = this.visit(expression.right);
			builder.append(b);
		}
		if (expression.op != null) {
			switch(expression.op.getType()) {
			case ExprParser.DIVIDE: builder.append("/"); eval = a / b; break;
			case ExprParser.TIMES: builder.append("*"); eval = a * b; break;
			case ExprParser.PLUS: builder.append("+"); eval = a + b; break;
			case ExprParser.MINUS: builder.append("-"); eval = a - b; break;
			}
		}
		
		return eval;
	}

}
